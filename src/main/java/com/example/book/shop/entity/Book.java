package com.example.book.shop.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;


@Entity
public class Book {

    @Id
    private Long id;

    private String name;
    private String brand;
    private Cover cover;
    private String author;
    private Integer count;
}
